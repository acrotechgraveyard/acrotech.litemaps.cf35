﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Acrotech.LiteMaps
{
    public class ManualResetEventSlim
    {
        public ManualResetEventSlim(bool initialState)
        {
            WaitHandle = new ManualResetEvent(initialState);
        }

        private ManualResetEvent WaitHandle { get; set; }

        public bool Wait(int millisecondsTimeout)
        {
            return WaitHandle.WaitOne(millisecondsTimeout, false);
        }
    }
}
