﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace System.IO
{
    public static class PathCompat
    {
        public static string Combine(params string[] args)
        {
            return Combine(args, 0);
        }

        private static string Combine(string[] args, int index)
        {
            string result = string.Empty;

            if (index < args.Length)
            {
                result = Path.Combine(args[index], Combine(args, index + 1));
            }

            return result;
        }
    }
}
